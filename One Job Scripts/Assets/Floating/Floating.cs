using System.Collections;
using UnityEngine;

public class Floating : MonoBehaviour
{
	public enum FloatAxis
	{
		X, Y, Z
	}

	[SerializeField, Min(0f)] private FloatAxis _axis = FloatAxis.Y;
	[SerializeField, Min(0f)] private float _floatSpeed = 200f;
	[SerializeField, Min(0f)] private float _floatDistance = 0.5f;

	private float _sinusAngle = 0f;
	private Vector3 _initialPosition;

	private void Start()
	{
		_initialPosition = transform.localPosition;
	}

	private void Update()
	{
		UpdateSinusAngle();
		UpdatePosition();
	}

	private void UpdateSinusAngle()
	{
		_sinusAngle += Time.deltaTime * _floatSpeed;
		if (_sinusAngle >= 360f) _sinusAngle = 0f;
	}

	private void UpdatePosition()
	{
		float multiplier = Mathf.Sin(Mathf.Deg2Rad * _sinusAngle);
		Vector3 pos = _initialPosition;

		switch (_axis)
		{
			case FloatAxis.X:
				pos.x += multiplier * _floatDistance;
				break;
			case FloatAxis.Y:
				pos.y += multiplier * _floatDistance;
				break;
			case FloatAxis.Z:
				pos.z += multiplier * _floatDistance;
				break;
			default:
				break;
		}

		transform.localPosition = pos;
	}
}