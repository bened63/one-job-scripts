using System.Collections;
using UnityEngine;

public class Spawner2D : MonoBehaviour
{
	[Header("Settings")]
	[SerializeField] private string _prefabName;
	[SerializeField] private GameObject _entityToSpawn;
	[SerializeField, Min(0)] public int _numberOfPrefabsToSpawn;
	[SerializeField, Min(0)] private float _spawnRadius;
	[SerializeField, Range(0f, 1f)] private float _spawnDelayFactor;
	[SerializeField] private Color _gizmoColor = Color.red;

	[Header("Respawn Settings")]
	[SerializeField] private bool _respawn = false;
	[SerializeField] private float _respawnTime = 5;

	//void SpawnEntities()
	//{
	//	int currentSpawnPointIndex = 0;

	//	for (int i = 0; i < spawnManagerValues.numberOfPrefabsToCreate; i++)
	//	{
	//		// Creates an instance of the prefab at the current spawn point.
	//		GameObject currentEntity = Instantiate(entityToSpawn, spawnManagerValues.spawnPoints[currentSpawnPointIndex], Quaternion.identity);

	//		// Sets the name of the instantiated entity to be the string defined in the ScriptableObject and then appends it with a unique number.
	//		currentEntity.name = spawnManagerValues.prefabName + instanceNumber;

	//		// Moves to the next spawn point index. If it goes out of range, it wraps back to the start.
	//		currentSpawnPointIndex = (currentSpawnPointIndex + 1) % spawnManagerValues.spawnPoints.Length;

	//		instanceNumber++;
	//	}
	//}

	// Start is called before the first frame update
	private void Start()
	{
		SpawnEntities();
	}

	private void SpawnEntities()
	{
		StartCoroutine(IESpawnEntities());
	}

	private IEnumerator IESpawnEntities(float delayBeforeStart = 0f)
	{
		yield return new WaitForSeconds(delayBeforeStart);

		bool run = true;

		// This will be appended to the name of the created entities and increment when each is created
		int instanceNumber = 1;

		while (run)
		{
			for (int i = 0; i < _numberOfPrefabsToSpawn; i++)
			{
				float delay = Random.Range(0.01f, 0.5f) * _spawnDelayFactor;
				yield return new WaitForSeconds(delay);

				// Position inside a sphere with radius {_spawnRadius} and the center at zero
				Vector3 pos = Random.insideUnitCircle * _spawnRadius;

				// Add offset
				pos += transform.position;

				// Spawn GameObject
				Quaternion rotation = Quaternion.Euler(0, 0, Random.Range(0.0f, 360.0f));

				// Create an instance of the prefab
				GameObject go = Instantiate(_entityToSpawn, pos, rotation) as GameObject;

				// Set the name of the instantiated entity
				go.name = string.Format("{0}_{1}", _prefabName, instanceNumber.ToString("00"));

				// Set parent
				go.transform.parent = transform;

				// Increase instance number
				instanceNumber++;
			}

			run = _respawn;
			yield return new WaitForSeconds(_respawnTime);
		}
	}

	private void OnDrawGizmos()
	{
		//Gizmos.color = new Color(1, 1, 1, 0.25f) * _gizmoColor;
		//Gizmos.DrawSphere(transform.position, _spawnRadius);
		//Gizmos.DrawWireSphere(transform.position, _spawnRadius);

#if UNITY_EDITOR
		UnityEditor.Handles.color = Color.red;
		UnityEditor.Handles.DrawWireDisc(transform.position, Vector3.forward, _spawnRadius);
#endif
	}
}

